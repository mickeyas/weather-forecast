import React from 'react';
import HomeScreen from './src/screens/HomeScreen';
import MyLocationScreen from './src/screens/MyLocationScreen';
import SettingSceen from './src/screens/SettingScreen';
import { ThemeProvider } from 'styled-components';
import SearchScreen from './src/screens/SearchScreen';
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import WeatherReducer from './src/reducer/WeatherReducer';
import AddressReducer from './src/reducer/AddressReducer';
import LocationReducer from './src/reducer/LocationReducer';
import SettingReducer from './src/reducer/SettingReducer';
import { useSelector } from "react-redux";
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { FontAwesome } from '@expo/vector-icons';
import { createNativeStackNavigator } from '@react-navigation/native-stack';




// const mainFlow = createBottomTabNavigator({
//   Home: HomeScreen,
//   MyLocation: MyLocationScreen,
//   SettingTheme: SettingSceen,
// }, {
//   initialRouteName: 'Home',
//   tabBarOptions: {
//     style: {
//       backgroundColor: '#fff',
//     },
//   }
// }
// );

// const SearchLocation = createStackNavigator({
//   Search: SearchScreen,
// });



// mainFlow.navigationOptions = {
//   headerShown: false,
// };

// SearchLocation.navigationOptions = {
//   title: '',
// };



// const switchNavigator = createStackNavigator({
//   mainFlow,
//   SearchLocation,
// });


//const App = createAppContainer(switchNavigator);


const persistConfig = {
  key: "root",
  version: 1,
  storage: AsyncStorage,
  blacklist: ["address"],
};

const rootReducer = combineReducers({
  weather: WeatherReducer,
  locations: LocationReducer,
  setting: SettingReducer,
  address: AddressReducer,
});

const persistedReducers = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducers,
  middleware: (getDefaultMidleware) =>
    getDefaultMidleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
      immutableCheck: {
        ignoredPaths: ['ignoredPath', 'ignoredNested.one', 'ignoredNested.two'],
      },
    }),
});

let persistor = persistStore(store);

function HomeTabs() {
  const settingState = useSelector((state) => state.setting);
  const Tab = createMaterialBottomTabNavigator();
  return (
    <Tab.Navigator
      activeColor="#e91e63"
      barStyle={{ backgroundColor: settingState.theme.colorBc }}>
      <Tab.Screen name="Main" component={HomeScreen} options={{

        tabBarIcon: () => (
          <FontAwesome name="home" size={20} color='gray' />
        ),
      }} />
      <Tab.Screen name="MyLocations" component={MyLocationScreen} options={{

        tabBarIcon: () => (
          <FontAwesome name="map-marker" size={20} color='gray' />
        ),
      }} />
      <Tab.Screen name="Settings" component={SettingSceen}
        options={{

          tabBarIcon: () => (
            <FontAwesome name="gear" size={20} color='gray' />
          ),
        }} />
    </Tab.Navigator>
  );
}

function AppContainer() {
  const Tab = createMaterialBottomTabNavigator();
  const Stack = createNativeStackNavigator();
  const settingState = useSelector((state) => state.setting);

  return <ThemeProvider theme={settingState.theme}>
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen options={{ headerShown: false }} name="Home" component={HomeTabs} />
        <Stack.Screen options={{
          title: '', headerStyle: {
            backgroundColor: settingState.theme.colorBc,
          },
        }} name="Search" component={SearchScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  </ThemeProvider>
}




export default () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppContainer />
      </PersistGate>
    </Provider>
  );
};
