import styled from "styled-components/native";




export const Wrapper = styled.View`
    background: ${props => props.theme.background};
    flex: 1;
    

`;

export const TextSt = styled.Text`
    color: ${props => props.theme.color};
    font-size: 20px;
    
`;

export const TextCard = styled.Text`
    color: ${props => props.theme.colorCard};
    font-size: 20px;
    
`;

export const TextBt = styled.Text`
    color: ${props => props.theme.color};
    font-size: 15px;
    
`;

export const TextHeader = styled.Text`
    color: ${props => props.theme.colorCard};
    font-size: 25px;

`;

export const ButtonCard = styled.TouchableOpacity`
  background: ${props => props.theme.colorBt};
  border-radius: 5px;
  flex: 1;
  margin: 1px;
  padding: 0.25px 3px;
`;

export const ContainerGoogle = styled.View`
    background: ${props => props.theme.background};
    flex: 1;
    padding: 10px;
    paddingTop: 60px;
`;


export const CardView = styled.View`
    borderRadius: 6px;
    elevation: 3;
    background: ${props => props.theme.colorBc};
    marginHorizontal: 4px;
    marginVertical: 6px;

`;

export const CardHeader = styled.View`
  
    elevation: 3;
    background: ${props => props.theme.colorBc};
    marginHorizontal: 4px;
    marginVertical: 6px;
    padding: 0px;
    margin: 0px; 
    marginTop: 40px;
    alignItems: center
`;