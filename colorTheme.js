export const lightTheme = {
    isEnable: false,
    background: '#ecf0f1',
    color: '#000',
    colorCard: '#000',
    colorBt: '#7590FF',
    colorBc: '#fff',
};

export const darkTheme = {
    isEnable: true,
    background: '#001',
    color: '#fff',
    colorCard: '#AD48C6',
    colorBt: '#AD48C6',
    colorBc: '#29293d',
};