import { createSlice } from "@reduxjs/toolkit";

const locationsSlice = createSlice({
    name: "locations",
    initialState: {
        locations: []
    },
    reducers: {
        saveLocation: (state, action) => {
            if (action.payload != '') {
                const location = {
                    id: Math.floor(Math.random() * 99999),
                    address: action.payload
                }
                state.locations.push(location);
            }
        },
        deleteLocation: (state, action) => {
            state.locations = state.locations.filter((location) => location.id !== action.payload);
        },
    },
});

export const { saveLocation, deleteLocation } = locationsSlice.actions;

export default locationsSlice.reducer;
