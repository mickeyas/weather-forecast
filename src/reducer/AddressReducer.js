import { createSlice } from "@reduxjs/toolkit";

const addressSlice = createSlice({
    name: "address",
    initialState: {
        address: ''
    },
    reducers: {
        saveAddress: (state, action) => {
            state.address = action.payload;
        },
    },
});

export const { saveAddress } = addressSlice.actions;

export default addressSlice.reducer;
