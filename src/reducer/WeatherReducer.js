import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import weatherApi from '../api/weather'

export const getWeathers = createAsyncThunk(
    "WeatherReducer/getWeathers",
    async (location) => {
        try {
            const { lat, lon } = location;
            const response = await weatherApi.get(`/onecall`, {
                params: {
                    lat: lat,
                    lon: lon,
                    exclude: 'currently,minutely,hourly,alerts'

                }
            });

            return response.data;
        } catch (err) {
            return err;
        }

    }
);

const weatherSlice = createSlice({
    name: "weather",
    initialState: { weathers: [], dailys: [], dateTimes: [], temperatures: [], symbolT: '°F', isLoading: true, isFahr: true },
    reducers: {
        getDailys: (state) => {
            // console.log('--getdailyka--');
            // console.log(state.weathers.daily);
            const result = state.weathers?.daily?.slice(0, 6).map(item => {
                //console.log(item.weather);
                return item.weather[0]
            });

            if (result != undefined) {
                state.dailys = result;
            }

        },
        getDateTimes: (state) => {
            const result = state.weathers?.daily?.slice(0, 6).map(item => {

                var dates = new Date(item.dt * 1000);
                var dateTime = `${dates.getDate()}/${dates.getMonth()}/${dates.getFullYear()}`;

                return dateTime;
            });

            if (result != undefined) {
                state.dateTimes = result;
            }
        },
        getTemperature: (state) => {
            const result = state.weathers?.daily?.slice(0, 6).map(item => {

                return parseFloat(item.temp.max - 273.15).toFixed(2);
            });

            if (result != undefined) {
                state.temperatures = result;
            }
        },
        changeTemperature: (state) => {

            if (state.isFahr) {
                const result = state.temperatures.map(fahrht => {
                    var myNumber = (fahrht - 32) / 1.8;
                    var myNumberWithTwoDecimalPlaces = parseFloat(myNumber).toFixed(2);
                    return myNumberWithTwoDecimalPlaces;
                });
                state.temperatures = result;
                state.isFahr = !state.isFahr;
                state.symbolT = '°C';

            }
            else {
                const result = state.temperatures.map(fahrht => {
                    var myNumber = (fahrht * 1.8) + 32;
                    var myNumberWithTwoDecimalPlaces = parseFloat(myNumber).toFixed(2);
                    return myNumberWithTwoDecimalPlaces;
                });
                state.temperatures = result;
                state.isFahr = !state.isFahr;
                state.symbolT = '°F';
            }
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getWeathers.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(getWeathers.fulfilled, (state, action) => {
                state.isLoading = false;
                state.weathers = action.payload;
            })
            .addCase(getWeathers.rejected, (state, action) => {
                state.isLoading = false;
            });
    },
});

export const { getDailys, getDateTimes, getTemperature, changeTemperature } = weatherSlice.actions;

export default weatherSlice.reducer;
