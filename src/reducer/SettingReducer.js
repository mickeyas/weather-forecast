import { createSlice } from "@reduxjs/toolkit";
import { darkTheme, lightTheme } from '../../colorTheme';

const settingSlice = createSlice({
    name: "setting",
    initialState: {
        theme: lightTheme, isLight: false
    },
    reducers: {
        switchTheme: (state) => {
            if (state.isLight) {
                // console.log(darkTheme);
                state.theme = lightTheme;
                state.isLight = false;
            }
            else {
                state.theme = darkTheme;
                state.isLight = true;
            }
        },
    },
});

export const { switchTheme } = settingSlice.actions;

export default settingSlice.reducer;
