import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import CurrentWeather from './CurrentWeather';



const DailyWeather = ({ dailysWeather, temps, datesTime, symbol }) => {




    return (
        <FlatList
            data={dailysWeather}
            keyExtractor={(daily, index) => index}
            renderItem={({ item, index }) => {
                if (index >= 0 && index <= 5) {
                    return <CurrentWeather timezone={''}
                        temp={temps[index]}
                        description={item.weather[0].description}
                        icon={item.weather[0].icon}
                        dateTime={datesTime[index]}
                        symbol={symbol}
                        pressure={item.pressure}
                        humidity={item.humidity}
                        wind={item.wind_speed}
                    />
                }


            }}
        />
    );
};



const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 100,

    },
    con: {
        alignItems: 'center',
    }
});

export default DailyWeather;