import React from 'react';
import { StyleSheet } from 'react-native';
import { TextHeader, CardHeader } from '../../Theme';



const Header = ({ title }) => {


    return (
        <CardHeader>
            <TextHeader>{title}</TextHeader>
        </CardHeader>
    );
};



const styles = StyleSheet.create({});

export default Header;