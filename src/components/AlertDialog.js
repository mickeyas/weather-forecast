import { StyleSheet, Alert } from 'react-native';



const AlertDialog = (title, detail, onPress) => {
    Alert.alert(
        title,
        detail,
        [
            {
                text: "Cancel",

            },
            { text: "OK", onPress: onPress }
        ]
    )
};



const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },

});

export default AlertDialog;