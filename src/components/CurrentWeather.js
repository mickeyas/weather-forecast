import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { TextCard, ButtonCard, TextBt, CardView } from '../../Theme';



const CurrentWeather = ({ timezone, temp, symbol, description, icon, onChangeTemp, isShowBt, dateTime, pressure, humidity, wind }) => {




    return (
        <CardView>
            <View style={styles.cardContent}>
                {timezone == '' ? null : <TextCard>{timezone}</TextCard>}
                <TextCard>{dateTime}</TextCard>
                <View style={styles.con}>
                    <Image style={styles.image} source={{ uri: `http://openweathermap.org/img/wn/${icon}@2x.png` }} />
                </View>
                <View style={styles.container}>
                    <TextCard>{`${temp}${symbol}`}</TextCard>
                    {isShowBt == true ?
                        <View style={styles.containerBt}>
                            <ButtonCard
                                onPress={onChangeTemp}
                            >
                                <TextBt style={styles.text}>Change temperature</TextBt>
                            </ButtonCard>
                        </View>
                        : null}
                </View>
                <TextCard>{description}</TextCard>
                {pressure == null ? null : <TextCard>{`Pressure: ${pressure}`}</TextCard>}
                {humidity == null ? null : <TextCard>{`Humidity: ${humidity}`}</TextCard>}
                {humidity == null ? null : <TextCard>{`Wind: ${wind}`}</TextCard>}
            </View>
        </CardView>
    );
};



const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 100,

    },
    con: {
        alignItems: 'center',
    },
    container: {

        flexDirection: 'row',
        justifyContent: 'space-between',


    },
    containerBt: {
        flex: 0.85,
        //textAlign: 'center',
        alignContent: 'center',
    },
    text: {
        fontSize: 12,
        textAlign: 'center',
    },
    button: {
        backgroundColor: 'red',
        color: 'red',
        fontSize: 10,
    },
    card: {
        borderRadius: 6,
        elevation: 3,
        backgroundColor: '#fff',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        shadowRadius: 2,
        marginHorizontal: 4,
        marginVertical: 6,
    },
    cardContent: {
        marginHorizontal: 18,
        marginVertical: 10,
    }

});

export default CurrentWeather;