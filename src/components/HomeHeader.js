import React, { useContext } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Alert } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { TextHeader, CardHeader } from '../../Theme';
import { Card } from 'react-native-elements';
import AlertDialog from './AlertDialog';


const HomeHeader = ({ navigation, title, saveLocation }) => {




    return (
        <CardHeader >
            <View style={styles.container}>
                <TextHeader>{title}</TextHeader>
                <View style={styles.iconcon}>
                    <TouchableOpacity
                        onPress={navigation}
                    >
                        <FontAwesome name="search" size={20} style={styles.icon} />
                    </TouchableOpacity>



                    <TouchableOpacity
                        onPress={() => AlertDialog('Save location', 'Do you want to save location?', saveLocation)}
                    >
                        <FontAwesome name="map-marker" size={20} style={styles.icon2} />
                    </TouchableOpacity>
                </View>


                {/* <TouchableOpacity
                    onPress={() => {
                    }}
                >
                    <FontAwesome name="refresh" size={20} style={styles.icon} />
                </TouchableOpacity> */}
            </View>
        </CardHeader>
    );
};



const styles = StyleSheet.create({
    container: {

        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    icon2: {
        paddingLeft: 10,
    },
    iconcon: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '20%',
        height: '65%',

        marginLeft: 10,
    },

});

export default HomeHeader;