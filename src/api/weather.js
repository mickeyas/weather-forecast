import axios from "axios";


export default axios.create({
    baseURL: 'https://api.openweathermap.org/data/2.5',
    params: {
        appid: 'dd0e554828d397a9a1c53216d9493b97',
    }
});

