import React from 'react';
import { StyleSheet } from 'react-native';
import { Wrapper, ContainerGoogle } from '../../Theme';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { saveAddress } from '../reducer/AddressReducer';
import { useDispatch } from "react-redux";

const GOOGLE_PLACES_API_KEY = 'AIzaSyA_qD6lhMIfvaVXtn6NO5J4e5wRHc2NxTA';
const SearchScreen = ({ navigation }) => {

    const dispatch = useDispatch();

    return (
        <Wrapper>
            <ContainerGoogle>
                <GooglePlacesAutocomplete
                    placeholder="Search"
                    query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                    }}
                    onPress={(data, details = null) => {

                        dispatch(saveAddress(data.terms[0].value));

                        navigation.pop();
                    }}
                    onFail={(error) => console.error(error)}

                />
            </ContainerGoogle>

        </Wrapper>


    );
};

SearchScreen.navigationOptions = () => {
    return {
        header: () => false,
    };
};



const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingTop: 60,

    },
});

export default SearchScreen;