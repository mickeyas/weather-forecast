import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { Wrapper, TextSt } from '../../Theme';
import HomeHeader from '../components/HomeHeader';
import CurrentWeather from '../components/CurrentWeather';
import DailyWeather from '../components/DailyWeather';
import useWeather from '../hook/useWeather';
import useTemperature from '../hook/useTemperature';
import useDatetime from '../hook/useDatetime';
import { useDispatch, useSelector } from "react-redux";
import { getWeathers, changeTemperature } from '../reducer/WeatherReducer';
import { saveLocation } from '../reducer/LocationReducer';






const HomeScreen = ({ navigation }) => {

    const weatherState = useSelector((state) => state.weather);
    const addressState = useSelector((state) => state.address);
    const dispatch = useDispatch();
    const [err] = useWeather(getWeathers);
    useDatetime();
    useTemperature();


    return (weatherState.isLoading)
        ? <View style={styles.container}><TextSt>LOADING ... </TextSt></View> : (
            <Wrapper >
                <HomeHeader navigation={() => {
                    navigation.navigate('Search');
                }} title={'Weather Forecast'}
                    saveLocation={() => {
                        dispatch(saveLocation(addressState.address));
                    }}
                />



                <CurrentWeather
                    timezone={weatherState.weathers.timezone}
                    dateTime={weatherState.dateTimes[0]}
                    temp={weatherState.temperatures[0]}
                    symbol={weatherState.symbolT}
                    description={weatherState.weathers.daily[0].weather[0].description}
                    icon={weatherState.weathers.daily[0].weather[0].icon}
                    onChangeTemp={() => {
                        dispatch(changeTemperature());
                    }}
                    isShowBt={true}
                />


                <DailyWeather
                    dailysWeather={weatherState.weathers.daily}
                    temps={weatherState.temperatures}
                    datesTime={weatherState.dateTimes}
                    symbol={weatherState.symbolT}
                />

                {/* <TextSt>{state.weather.daily[0].dt}</TextSt>
            <TextSt>{state.errorMessage}</TextSt> */}
                {err ? <TextSt>{err}</TextSt> : null}
            </Wrapper>
        );
};


HomeScreen.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarIcon: <FontAwesome name="home" size={20} color='gray' />,

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        backgroundColor: 'red',
        color: 'blue',
    }
});

export default HomeScreen;