import React from 'react';
import { View, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { FontAwesome } from '@expo/vector-icons';
import { Wrapper, } from '../../Theme';
import Header from '../components/Header';
import { TextCard, CardView } from '../../Theme';
import { deleteLocation } from '../reducer/LocationReducer';
import { saveAddress } from '../reducer/AddressReducer';
import AlertDialog from '../components/AlertDialog';


const MyLocationScreen = ({ navigation }) => {
  const locationState = useSelector((state) => state.locations);
  const dispatch = useDispatch();

  return (locationState.locations.length == 0) ? <Wrapper><Header title={'Weather Forecast'} /></Wrapper> :
    (


      <Wrapper >
        <Header title={'Weather Forecast'} />
        <FlatList
          data={locationState.locations}
          keyExtractor={(location) => location.id}
          renderItem={({ item, index }) => {
            return <TouchableOpacity onPress={() => {
              navigation.navigate('Main');
              dispatch(saveAddress(item.address));
            }}>
              <CardView>
                <View style={styles.container}>
                  <TextCard>{item.address}</TextCard>
                  <TouchableOpacity
                    onPress={() => {
                      AlertDialog('Delete location', 'Do you want to delete location?', () => dispatch(deleteLocation(item.id)));
                    }}
                  >
                    <FontAwesome name="trash" size={20} style={styles.icon} />
                  </TouchableOpacity>
                </View>
              </CardView>
            </TouchableOpacity>
          }}
        />
      </Wrapper >


    );
};

MyLocationScreen.navigationOptions = {
  title: 'MyLocation',
  tabBarIcon: <FontAwesome name="map-marker" size={20} color='gray' />
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 18,
    marginVertical: 10,
  },

  icon: {
    padding: 15,
  },


});

export default MyLocationScreen;