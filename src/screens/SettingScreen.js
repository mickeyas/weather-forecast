import React from 'react';
import { View, StyleSheet, Switch } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { Wrapper, TextSt } from '../../Theme';
import Spacer from '../components/Spacer';
import Header from '../components/Header';
import { useSelector, useDispatch } from "react-redux";
import { switchTheme } from '../reducer/SettingReducer';

const SettingSceen = () => {

  const settingState = useSelector((state) => state.setting);
  const dispatch = useDispatch();


  return (
    <Wrapper >
      <Header title={'Weather Forecast'} />
      <View style={styles.container}>
        <Spacer>
          <TextSt>Dark mode</TextSt>
        </Spacer>
        <Switch
          style={styles.switch}
          onValueChange={() => {
            dispatch(switchTheme());
          }}
          value={settingState.isLight} />
      </View>
    </Wrapper>
  );
};

SettingSceen.navigationOptions = {
  title: 'Setting',
  tabBarIcon: <FontAwesome name="gear" size={20} color='gray' />
};

const styles = StyleSheet.create({
  container: {


    // flex: 1,
    padding: 20,
    flexDirection: 'row',

  },
  switch: {

    flex: 1,
    marginTop: 5,
    alignItems: 'flex-end',

  },
});

export default SettingSceen;