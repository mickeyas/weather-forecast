import { useState, useEffect } from 'react';
import {


    requestForegroundPermissionsAsync,
    getCurrentPositionAsync,

} from 'expo-location';
import Geocoder from 'react-native-geocoding';
import { useSelector, useDispatch } from "react-redux";



export default (getWeather) => {
    const addressState = useSelector((state) => state.address);
    const [err, setErr] = useState(null);
    //const { state } = useContext(LocationContext);
    const GOOGLE_PLACES_API_KEY = 'AIzaSyA_qD6lhMIfvaVXtn6NO5J4e5wRHc2NxTA';
    const dispatch = useDispatch();
    Geocoder.init(GOOGLE_PLACES_API_KEY);




    useEffect(() => {
        const requestGetWeather = async () => {
            try {
                const { granted } = await requestForegroundPermissionsAsync();
                if (!granted) {
                    throw new Error('Location permission not granted');
                }
                else {
                    if (addressState.address != '') {
                        Geocoder.from(addressState.address)
                            .then(json => {
                                var location = json.results[0].geometry.location;
                                const locationc = {
                                    lat: location.lat,
                                    lon: location.lng
                                }
                                dispatch(getWeather(locationc));
                            })
                            .catch((e) => {
                                setErr(e.message);
                            });


                    }
                    else {
                        let currentLocation = await getCurrentPositionAsync({});
                        const location = {
                            lat: currentLocation.coords.latitude,
                            lon: currentLocation.coords.longitude
                        }
                        console.log(location);
                        dispatch(getWeather(location));
                    }

                }

            } catch (e) {
                console.log(e.message);
                setErr(e.message);
            }
        };

        requestGetWeather();
    }, [addressState.address]);

    return [err];
};

